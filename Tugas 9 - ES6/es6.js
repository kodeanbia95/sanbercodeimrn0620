// const golden = function goldenFunction(){
//   console.log("this is golden!!")
// }
// golden()

console.log("no 1")
const golden = () => {
    console.log("this is golden!!")
}
golden()

console.log("no 2")
const newFunction = function literal(firstName, lastName){
  return {
    // firstName: firstName,
    // lastName: lastName,
    firstName,
    lastName,
    fullName: function(){
      console.log(firstName + " " + lastName)
      return 
    }
  }
}
 
//Driver Code 
newFunction("William", "Imoh").fullName() 

console.log("no 3")
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

const {firstName, lastName,destination,occupation} = newObject
console.log(firstName, lastName, destination, occupation)


console.log("no 4")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)
const combined = [...west,...east]
//Driver Code
console.log(combined)


console.log("no 5")
const planet = "earth"
const view = "glass"
var before = 'Lorem ' + view + 'dolor sit amet, ' +  
    'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'
var beforee = `Losrem ${view} dolor sit amet,
consectetur adipiscing elit, ${planet} do eiusmod tempor 
incididunt ut labore et dolore magna aliqua. Ut enim 
ad minim veniam`
// Driver Code
console.log(beforee) 