//Tugas 3 - Looping while, No 1
var angka = 1
var pesan = "I love Coding"
console.log("Looping Pertama".toUpperCase())
while(angka < 20) {
    angka++
    if(angka%2==0){
        console.log(angka," - ",pesan)
    }
}
console.log("Looping Kedua".toUpperCase())
angka = 20
pesan = "I will become a mobile developer"
while(angka > 1){
    angka--
    if(angka%2==0){
        console.log(angka, " - ", pesan)
    }
}

// Tugas No 2
console.log("\n")
var pesanA=" Santai"
var pesanB=" Berkualitas"
var pesanC=" I Love Coding"
for(var i = 1; i <= 20; i++){
    if(i % 2 == 0){
        console.log(i + pesanB)    
    } else if(i % 3 == 0){
        console.log(i + pesanC)
    } else {
        console.log(i + pesanA)
    }
}

// Tugas No 3
console.log("\n")
var tanda = "#"
for(var a=0; a<4; a++){
    var spasi = ""
    for(var b=0; b<8; b++){
        spasi = spasi+tanda
    }
    console.log(spasi)
}

// Tugas No 4
console.log("\n")
var pagar = "#"
var jarak =""
var x = 1
for(x; x<=1; x++){
    for(x; x<=7; x++){
        jarak+=pagar
        console.log(jarak)
    }
}

// Tugas No 5
console.log("\n")
var tanda = "# "
for(var a=0; a<4; a++){
    var spasi = "\n"
    for(var b=0; b<4; b++){
        if(a%2 == 0){
            spasi = spasi+" #"    
        } else {
            spasi = spasi+"# "
        }
    }
    console.log(spasi)
}
