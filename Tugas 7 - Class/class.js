class Animal {
    constructor(name){
        this.name = name
        this.legs = 4
        this.cold_blooded = false
    }
}

var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

class Frog extends Animal{
    jump(){
        return "hop hop"
    }
}

class Ape extends Animal{
    yell(){
        return "Auoo"
    }
}

var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 
console.log(kodok.jump())

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
console.log(sungokong.yell())


// function Clock({ template }) {
  
//   var timer;

//   function render() {
//     var date = new Date();

//     var hours = date.getHours();
//     if (hours < 10) hours = '0' + hours;

//     var mins = date.getMinutes();
//     if (mins < 10) mins = '0' + mins;

//     var secs = date.getSeconds();
//     if (secs < 10) secs = '0' + secs;

//     var output = template
//       .replace('h', hours)
//       .replace('m', mins)
//       .replace('s', secs);

//     console.log(output);
//   }

//   this.stop = function() {
//     clearInterval(timer);
//   };

//   this.start = function() {
//     render();
//     timer = setInterval(render, 1000);
//   };

// }

// var clock = new Clock({template: 'h:m:s'});
// clock.start(); 


class Clock {
    constructor ({template}){
        this.template = template
    }
    render(){
        var date = new Date()
        this.hour = date.getHours()
        if(this.hour<10) this.hour = `0${this.hour}`

        this.mins = date.getMinutes()
        if(this.mins<10) this.mins = `0${this.mins}`
        
        this.secs = date.getSeconds()
        if(this.secs<10) this.secs = `0${this.secs}`
        
        this.output = this.template.replace('h', this.hour).replace('m', this.mins).replace('s', this.secs)
        console.log(this.output)
    }

    stop(){
        clearInterval(this.timer)
    }

    start(){
        this.render()
        // this.timer = setInterval(this.render.bind(this),1000)
        this.timer = setInterval(()=> this.render(),1000)
    }
    
}

var clock = new Clock({template: 'h:m:s'});
clock.start();  
