import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import YoutubeUI from './Tugas/Tugas12/App';
import Index2 from './Tugas/Tugas13/index'
import HomeScreen from './Quiz3/HomeScreen'
import LoginScreen2 from './Quiz3/LoginScreen'
import Index from './Tugas/Tugas15/index'
import AppTugas14 from './Tugas/Tugas14/App'
import SkillScreen from './Tugas/Tugas14/SkillScreen'
import IndexNavigation from './Tugas/TugasNavigation/index'



export default class App extends React.Component {
  render(){
    return (
      // <SkillScreen/>
      // <AppTugas14/> //tugas 14
      <IndexNavigation/>
      // <Index2/> Tugas 13
      // <YoutubeUI /> Aplikasi Youtube UI Tugas 12
    );
  }
  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
