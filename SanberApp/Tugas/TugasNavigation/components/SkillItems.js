import React from 'react';
import {
    View, 
    Platform,
    Image, 
    StyleSheet,
    Text,
    TouchableOpacity
} from "react-native";
import Icon from 'react-native-vector-icons/MaterialIcons'; 
import MaterialCommunityIcons from "@expo/vector-icons/MaterialCommunityIcons";


export default class SkillItems extends React.Component {
    render(){
        return(
            <View>
                <View style={Styles.containerCard}>
                        <View style={{flexDirection:'row',justifyContent:'space-between',flex:1}}>
                            <View style={{marginTop:13, marginStart:5}}>
                                <View style={Styles.iconStyle}>
                                    <MaterialCommunityIcons style={{flex:1, marginTop:7,}} name={this.props.itemSkill.item.iconName} color='#045380' size={45}/>
                                </View>
                            </View>
                            <View style={{marginTop:20}}>
                                <Text style={Styles.titleSkill2}>{this.props.itemSkill.item.skillName}</Text>
                                <Text style={Styles.titleSkill2}>{this.props.itemSkill.item.categoryName}</Text>
                            </View>
                            <View style={{justifyContent:'center', marginEnd:5 }}>
                                <Text style={Styles.titleSkill3}>{this.props.itemSkill.item.percentageProgress}</Text>
                            </View>
                        </View>
                    </View>
                
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    containerCard:{
        height:90, 
        backgroundColor:'#045380',
        marginStart:15, 
        marginEnd:15, 
        marginTop:15, 
        borderRadius:10,
        borderColor:'#ffffff',
        borderBottomWidth:2,
        borderTopWidth:2,
        borderStartWidth:2,
        borderEndWidth:2 },
    iconStyle:{
        backgroundColor:'white', 
        padding:1, 
        borderRadius:10, 
        width:60,
        height:60,
        alignItems:'center'
    },
    titleSkill:{
        fontSize:14,
        fontWeight:'bold',
        color:'white',
        textAlign:'center'
    },
    titleSkill2:{
        marginTop:3,
        fontSize:16,
        fontWeight:'bold',
        color:'white',
        textAlign:'center'
    },
    titleSkill3:{
        marginTop:3,
        fontSize:32,
        fontWeight:'bold',
        color:'white',
        textAlign:'center'
    }
})