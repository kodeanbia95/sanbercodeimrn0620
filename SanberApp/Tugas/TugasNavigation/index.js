import * as React from 'react';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs"
import {createDrawerNavigator} from "@react-navigation/drawer"
import 'react-native-gesture-handler';

import LoginScreen from './LoginScreen';
import AboutScreen from './AboutScreen';
import SkillScrenn from "./SkillScreen"
import ProjectScreen from "./ProjectScreen"
import AddScreen from "./AddScreen"

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator()
const Tabs = createBottomTabNavigator()

const TabsScreen = () => (
  <Tabs.Navigator>
      <Tabs.Screen name='My Skill' component={SkillScrenn}/>
      <Tabs.Screen name='My Project' component={ProjectScreen}/>
      <Tabs.Screen name='Add Screen' component={AddScreen}/>
  </Tabs.Navigator>
)

const DrawerScreen = () => (
  <Drawer.Navigator>
      <Drawer.Screen name="About me" component={AboutScreen}/>
      <Drawer.Screen name="My Portofolio" component={TabsScreen}/>
  </Drawer.Navigator>
)

export default class App extends React.Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="LoginScreen" >
          <Stack.Screen name='LoginScreen' component={LoginScreen} options={{ headerShown: false }} />
          <Stack.Screen name='AboutScreen' component={DrawerScreen} options={{ headerShown: false }} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}