import React from 'react';
import {
    View,
    Image,
    StyleSheet,
    Text,
    Button,
    FlatList
} from "react-native";
import MaterialCommunityIcons from "@expo/vector-icons/MaterialCommunityIcons";
import SkillItems from './components/SkillItems'
import data from './skillData.json'


export default class SkillScreen extends React.Component {
    render(){
        return(
            <View style={{backgroundColor:'#B0B0B0', flex:1}}>
                <View style={{height:200, backgroundColor:'#045380', flex:1}}>
                    <View style={{flexDirection:'row', marginTop:25, marginStart:15, justifyContent:'space-between'}}>
                        <MaterialCommunityIcons 
                        name='menu' 
                        color='white' 
                        size={40}
                        onPress={() => this.props.navigation.toggleDrawer("DrawerScreen")}/>
                        <Image source={require('./image/logo.png')} style={{height:40,width:130,borderRadius:3}}/>
                        <MaterialCommunityIcons 
                        name='logout' color='white' 
                        size={40} style={{marginEnd:15}}
                        onPress={() => this.props.navigation.goBack()}/>
                    </View>            
                    <View style={{height:190, backgroundColor:'#ffffff', 
                    marginStart:15, marginEnd:15, marginTop:7, borderRadius:10,}}>
                        <View style={{flexDirection:'row'}}>
                            <Image source={{uri:'https://randomuser.me/api/portraits/men/0.jpg'}} 
                            style={{width:70, height:70, borderRadius:35, marginTop:30,marginStart:20}}/>
                            <View>
                                <Text style={{fontSize:18, marginTop:32,marginStart:40}}>Mobile Developer</Text>
                                <Text style={{fontSize:18, fontWeight:'bold', marginTop:10,marginStart:20}}>Ridho Anbia Habibullah</Text>
                            </View>
                        </View>
                        <View style={{paddingTop:30, paddingStart:100, paddingEnd:100}}>
                            <Button
                                title="Hiring me"
                                color="#045380"
                                hardwareAccelerated
                                alignSelf="center"
                                />
                        </View>
                    </View>
                    {/* <SkillItems iteem={data.items[2]}/> */}
                <FlatList
                        data={data.items}
                        renderItem={(itemSkill) => <SkillItems itemSkill={itemSkill}/>}
                        keyExtractor={(item) => item.id}
                        style={{marginTop:1, marginBottom:10}} ></FlatList>
                </View>
            </View>
            )
    
    } 
        
}

const Styles = StyleSheet.create({
    iconStyle:{
        backgroundColor:'white', 
        padding:1, 
        borderRadius:10, 
        width:60,
        height:60,
        alignItems:'center'
    },
    titleSkill:{
        fontSize:14,
        fontWeight:'bold',
        color:'white',
        textAlign:'center'
    },
    titleSkill2:{
        marginTop:3,
        fontSize:16,
        fontWeight:'bold',
        color:'white',
        textAlign:'center'
    },
    titleSkill3:{
        marginTop:3,
        fontSize:32,
        fontWeight:'bold',
        color:'white',
        textAlign:'center'
    }
})