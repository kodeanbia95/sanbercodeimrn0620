import React from 'react';
import {
    View,
    Image,
    StyleSheet,
    Text,
    Button
} from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from "@expo/vector-icons/MaterialCommunityIcons";


export default class LoginScreen extends React.Component {
    render(){
        return(
            <View style={{backgroundColor:'#B0B0B0', flex:1}}>
                <View style={{height:200, backgroundColor:'#045380'}}>
                    <View style={{flexDirection:'row', marginTop:25, marginStart:15, justifyContent:'space-between'}}>
                        <MaterialCommunityIcons 
                        name='menu' 
                        color='white' size={40} 
                        onPress={() => this.props.navigation.toggleDrawer("DrawerScreen")}/>
                        <Image source={require('./image/logo.png')} style={{height:40,width:130,borderRadius:3}}/>
                        <MaterialCommunityIcons 
                        name='logout' 
                        color='white' 
                        size={40} style={{marginEnd:15}}
                        onPress={() => this.props.navigation.goBack()}/>
                    </View>
                    <View style={{height:190, backgroundColor:'#ffffff', 
                    marginStart:15, marginEnd:15, marginTop:60, borderRadius:10,}}>
                        <View style={{flexDirection:'row'}}>
                            <Image source={{uri:'https://randomuser.me/api/portraits/men/0.jpg'}} 
                            style={{width:70, height:70, borderRadius:35, marginTop:30,marginStart:20}}/>
                            <View>
                                <Text style={{fontSize:18, marginTop:32,marginStart:40}}>Mobile Developer</Text>
                                <Text style={{fontSize:18, fontWeight:'bold', marginTop:10,marginStart:20}}>Ridho Anbia Habibullah</Text>
                            </View>
                        </View>
                        <View style={{paddingTop:30, paddingStart:100, paddingEnd:100}}>
                            <Button
                                title="Hiring me"
                                color="#045380"
                                hardwareAccelerated
                                alignSelf="center"
                                />
                        </View>
                    </View>
                    <View style={{paddingTop:25, paddingStart:7, paddingEnd:7, flexDirection:'row', justifyContent:'space-between'}}>
                        <View style={Styles.cardSocialMedia}>
                            <MaterialCommunityIcons name='facebook' color='white' size={40} 
                            style={{backgroundColor:'#045380', borderRadius:5, marginTop:7}}/>
                            <Text style={{textAlign:'center'}}>find me on Facebook</Text>
                            <Text style={{textAlign:'center'}}>(Ridho Anbia)</Text>
                        </View>
                        <View style={Styles.cardSocialMedia}>
                            <MaterialCommunityIcons name='instagram' color='white' size={40} 
                            style={{backgroundColor:'pink', borderRadius:5, marginTop:7}}/>
                            <Text style={{textAlign:'center'}}>follow me on Instagram</Text>
                            <Text style={{textAlign:'center'}}>(Ridho Anbia)</Text>
                        </View>
                        <View style={Styles.cardSocialMedia}>
                            <MaterialCommunityIcons name='twitter' color='#61B2E4' size={40} 
                            style={{backgroundColor:'white', borderRadius:5, marginTop:7}}/>
                            <Text style={{textAlign:'center'}}>follow me on Twitter</Text>
                            <Text style={{textAlign:'center'}}>(Ridho Anbia)</Text>
                        </View>
                    </View>
                    <View style={{height:150, backgroundColor:'#045380', 
                    marginStart:15, marginEnd:15, marginTop:15, borderRadius:10,}}>
                        <View style={{marginStart:9, marginTop:9, marginEnd:110}}>
                            <Text style={{fontSize:16, color:'white', borderBottomWidth:1, borderBottomColor:'white'}}>You can see my portofolio on</Text>
                        </View>
                        <View style={{flexDirection:'row'}}>
                            <View style={{marginTop:18, marginStart:5, alignItems:'center'}}>
                            <View style={Styles.iconStyle}>
                                    <Icon name='github' color='black' size={60}/>
                                </View>
                                <Text style={{textAlign:'center', color:'white', marginTop:9}}>@kodeanbia95</Text>
                            </View>
                            <View style={{marginTop:18, marginStart:20, alignItems:'center'}}>
                                <View style={Styles.iconStyle}>
                                    <Icon name='gitlab' color='orange' size={60}/>
                                </View>
                                <Text style={{textAlign:'center', color:'white', marginTop:9}}>@kodeanbia95</Text>
                            </View>
                            <View style={{marginTop:18, marginStart:20, alignItems:'center'}}>
                                <View style={Styles.iconStyle}>
                                    <Icon name='bitbucket' color='black' size={50}/>
                                </View>
                                <Text style={{textAlign:'center', color:'white', marginTop:9}}>@kodeanbia95</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
            )
        
    }
}

const Styles = StyleSheet.create({
    cardSocialMedia:{
        height:110,
        width:110,
        backgroundColor:'white', 
        borderRadius:10, 
        alignItems:'center',
        borderColor:'#045380',
        borderBottomWidth:2,
        borderTopWidth:2,
        borderStartWidth:2,
        borderEndWidth:2
    },
    iconStyle:{
        backgroundColor:'white', padding:1, borderRadius:30, width:60,height:60,alignItems:'center'
    }
})