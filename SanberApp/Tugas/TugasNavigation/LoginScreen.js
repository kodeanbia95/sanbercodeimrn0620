import React from 'react';
import {
    View, 
    Image, 
    StyleSheet,
    Text,
    TouchableOpacity,
    FlatList,
    SafeAreaView,
    TextInput,
    Button,
    Alert
} from "react-native";
import Icon from 'react-native-vector-icons/MaterialIcons'; 
 import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

export default class LoginScreen extends React.Component {
    constructor(props){
        super(props)
    }
    render(){
        return (
            <SafeAreaView style={{flex:1,backgroundColor:"#259FE3"}}>
                <View style={Styles.container}>
                    <View>
                        <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                            <View style={Styles.containerImage} >
                                <Image source={require('./image/logo.png')}></Image>
                            </View>
                        </View>
                        <View>
                            <Text style={Styles.appTitle}>Sanber App</Text>
                        </View>
                        <View>
                            <Text style={Styles.appText}>Sign in to continue</Text>
                        </View>
                        <View style={Styles.inputContainer}>
                            <TextInput 
                                style={Styles.textInput} 
                                placeholder='Email Address'/>
                        </View>
                        <View style={Styles.inputContainer}>
                            <TextInput 
                                style={Styles.textInput} 
                                placeholder='Password'
                                secureTextEntry={true}/>
                        </View>
                        <View>
                            <Text style={Styles.forgotText}>Forgot Password?</Text>
                        </View>
                        <View style={Styles.containerButton} alignSelf="center">
                            <Button
                                title="Login"
                                color="#045380"
                                hardwareAccelerated
                                alignSelf="center"
                                onPress={ () => this.props.navigation.navigate("AboutScreen")}
                                />
                        </View> 
                        <View  flexDirection='row'>
                            <Text style={Styles.styleText}>New User?</Text>
                            <Text style={Styles.styleText2}>Signup</Text>
                        </View>
                    </View>
                </View>
            </SafeAreaView>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex:1,
        paddingTop:23,
    },
    containerImage: {
        flex:1
    },
    appTitle:{
        paddingTop:30,
        paddingLeft:50,
        fontSize:30,
        color:"#03324B",
        fontWeight: 'bold'
    },
    appText:{
        paddingLeft:50,
        fontSize:18,
        paddingTop:3,
        color:"#ffffff"
    },
    forgotText:{
        paddingLeft:180,
        fontSize:18,
        paddingTop:3,
        color:"#ffffff"
    },
    styleText:{
        paddingLeft:90,
        fontSize:18,
        paddingTop:30,
        color:"#ffffff"
    },
    styleText2:{
        paddingLeft:20,
        fontSize:18,
        paddingTop:30,
        color:"#ffffff",
        fontWeight:'bold',
        color:"#03324B",
    },
    inputContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
        marginBottom: 16
      },
      labelText: {
        fontWeight: 'bold'
      },
      textInput: {
        borderRadius:3,
        marginTop:30,
        height:60,
        width: 300,
        paddingStart:20,
        backgroundColor: 'white',
        fontSize:18
      },
      containerButton:{
          paddingTop:30,
          width:300
      },
      buttonText: {
        fontSize: 24,
        fontWeight: "bold",
        color: "white"
    }
})