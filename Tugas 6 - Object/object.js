console.log("Jawaban Soal 1. \n")
var now = new Date()
var thisYear = now.getFullYear() // 2020 (tahun sekarang)

function arrayToObject(arr) {
    // Code di sini
    var object = {}
    for(var a = 0; a<arr.length; a++){
        object.firstName = arr[a][0],
        object.lastName = arr[a][1],
        object.gender = arr[a][2]
        object.age = arr[a][3]
        if(object.age == null || object.age>thisYear){
            object.age = "Invalid Birth day"
        } else {
            object.age =thisYear - arr[a][3]
        }
 
        console.log(`${a+1}. ${object.firstName} ${object.lastName} : {
        firstName:${object.firstName},
        lastName:${object.lastName},
        gender:${object.gender},
        age:${object.age}
        }`)
    }
    
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people)  
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""

console.log("\nJawaban Soal 2. \n")
function shoppingTime(memberId, money) {
    var dataObject = {}
    var arrBelajaan=[]
    var objBarang={
        "Sepatu brand Stacattu" : 1500000,
        "Baju Zoro" : 500000,
        "Baju H&N" : 250000,
        "Sweater Uniklooh" : 175000,
        "casing Handphone" : 50000
    }
    var hargaBarang = Object.values(objBarang)
    var sort = hargaBarang.sort(function(a,b){return b-a})
    var min = Math.min(...hargaBarang)
    var sisaUang = money
    var minMoney = 50000
    if(memberId == '' && money != null){
        return console.log(`Mohon maaf, toko X hanya berlaku untuk member saja`)
    } else if(memberId != null && money < minMoney) {
        return console.log(`Mohon maaf, uang tidak cukup`)
    } else if(memberId == null && money == null){
        return console.log(`Mohon maaf, toko X hanya berlaku untuk member saja`)
    } else if (memberId != null && money > minMoney ) {
        for (var i=0; i<hargaBarang[i]; i++){
            if(sisaUang>=sort[i]){
                arrBelajaan.push(Object.keys(objBarang)[Object.values(objBarang).indexOf(sort[i])])
                sisaUang -= sort[i]
            }
        }
        objeckBelanjaan = {
            memberId:memberId,
            money:money,
            listPurchased:arrBelajaan,
            changeMoney:sisaUang
        }
        return objeckBelanjaan
    }
}
 
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja



console.log("\nJawaban Soal 3. \n")
function naikAngkot(arrPenumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    for(var i= 0; i<rute.length; i++){
        
    }

    objPenumpang = {}
    for(var i = 0; i<=arrPenumpang.length; i++){
        objPenumpang.penumpang = arrPenumpang[i][0],
        objPenumpang.naikdari = arrPenumpang[i][1],
        objPenumpang.tujuan = arrPenumpang[i][2]
        
        console.log(objPenumpang)
    }

}
 
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
 
console.log(naikAngkot([])); //[]