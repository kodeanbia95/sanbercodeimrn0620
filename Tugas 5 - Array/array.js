//Jawaban no 1
console.log("\n No 2")
function range(starnum,finishnum){
    var arr = []
    if(starnum == null || finishnum == null){
        return -1
    } else if(starnum < finishnum){
        for(starnum; starnum <= finishnum; starnum++){
            arr.push(starnum)
        }
        return arr
    } else {
        for(starnum; starnum >= finishnum; starnum--){
            arr.push(starnum)
        }
        return arr
    }

}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

// jawaban no 2
console.log("\n No 2")
function rangeWithStep(starNumber, finishNumber, step){
    var array = []
    if(starNumber < finishNumber){
        for(starNumber; starNumber <= finishNumber;){
            array.push(starNumber)
            starNumber = starNumber+step
        }
        return array
    } else {
        for (starNumber; starNumber >= finishNumber;){
            array.push(starNumber)
            starNumber = starNumber-step
        }
        return array
    }
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

// jawaban no 3
console.log("\n No 3")
function sum(starNumber,finishNumber, step=1){
    var value = 0
    var arrayRange = rangeWithStep(starNumber,finishNumber,step)
    if(starNumber != null && finishNumber == null){
        return 1
    } else {
        for(var i=0; i< arrayRange.length; i++){
            value += arrayRange[i]
        }
        return value
    } 
    
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

// jawaban no 4
console.log("\n No 4")

//contoh input
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function dataHandling(){
    var x = 0
    for (x; x < input.length; x++){
        console.log("Nomoer ID: ",input[x][0])
        console.log("Nama Lengkap:",input[x][1])
        console.log("TTL: " ,input[x][2]," ",input[x][3])
        console.log("Hobi: ",input[x][4])
        console.log("\n")
    }
}
dataHandling()

// jawaban no 5
console.log("\n No 5")

function balikKata(input){
    var kata =" "
    var inputLength = input.length -1
    for(inputLength; inputLength >= 0; inputLength--){
        var inputan = input[0,inputLength];
        kata = kata + inputan
    }
    return kata
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 


// jawaban no 6
console.log("\n No 6")


function dataHandling2(array){
    array.splice(1,1,"Roman Alamsyah Elsharawy","Provinsi, Bandar Lampung","21/05/1989")
    array.splice(4, 4, "Pria", "SMA Internasional Metro");
    console.log(array);

    var tanggal = ""+array.slice(3,4);
    var bulan = tanggal[4]
    var strBulan;
    switch(bulan){
        case "5":{strBulan = "Mei";break}
    }
    console.log(strBulan)
    var tgl = array[3].split("/")
    var tglArr = []
    tglArr.push(tgl[2],tgl[0],tgl[1])
    console.log(tglArr)

    var strTglArr = []
    strTglArr.push(tgl[0],tgl[1],tgl[2])
    strTglArr2 = strTglArr.join("/")
    console.log(strTglArr2)

    var nickName = ""+array[1]
    var nickName2 = nickName.substr(0,14)
    console.log(nickName2)
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
 
/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 